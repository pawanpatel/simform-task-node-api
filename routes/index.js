import formRoutes from "./formRoutes"

export default (app) => {

    formRoutes(app)

    app.get('/', (request, response) => {
        response.send("Server is up and running")
    })
}