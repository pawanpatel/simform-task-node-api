import getAllFormsController from '../../controllers/formControllers/getAllForms.controller'
import createNewFormController from '../../controllers/formControllers/createNewForm.controller'
import getFormDetailsController from '../../controllers/formControllers/getForm.controller'

export default (app) => {
    app.get("/form/get-all-forms", getAllFormsController)
    app.post("/form/create-new-form", createNewFormController)
    app.get("/form/:formId", getFormDetailsController)
}