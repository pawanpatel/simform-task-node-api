import express from "express";
import appRoutes from './routes'
import cors from 'cors'

const app = express();
const PORT = process.env.PORT || 4200;

app.use(cors());
app.use(express.json());

appRoutes(app)

app.listen(PORT, () => {
    console.log("server up and running");
});