import mongojs from 'mongojs'
let db = null

db = mongojs(`mongodb+srv://temp-user:temp-password@cluster0.jukby.mongodb.net/test`);

db.on('connect', () => {
    console.log('DB Connected!')
})
db.on('error', (err) => {
    console.log(err)
})
export default db;